import sys
import socket
import requests
from concurrent.futures import ThreadPoolExecutor, thread

# output colors
SC, EC = '\033[91m', '\033[0m'

# scan type dict
scantype = {'easy': 1024, 'normal': 10000, 'full': 65535}

def portscan(ip, port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(0.5)
    res = s.connect_ex((ip, port))
    if res == 0:
        print(SC + '[+]' + EC + ' open port:', port)
    s.close()

def start_thread(ip, maxport):
    # i don't like pool
    try:
        with ThreadPoolExecutor(max_workers=3) as LAKE:
            for port in range(1, maxport+1):
                future = LAKE.submit(portscan, ip, port)
    except KeyboardInterrupt:
        LAKE._threads.clear()
        thread._threads_queues.clear()
        sys.exit('\n******* brutal exit *******')

if __name__ == '__main__':

    if not 1 < len(sys.argv) < 4:
        print('Usage: python3 portscan.py 10.10.10.10 easy/normal/full')
        exit()

    target = sys.argv[1]

    try:
        scan = scantype[sys.argv[2]]
    except:
        print('error on scan-type value, started scan on first 1024 ports')
        scan = scantype['easy']

    start_thread(target, scan)
    